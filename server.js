const express = require('express');

const app = express();

app.get('/', (req, res) => {
  res.json({
    ok: true,
  });
});

app.post('/login', (req, res) => {
  res.json({
    login: 'ok',
  });
});

app.listen(8000);
